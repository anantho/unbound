#!/usr/bin/env bash

# This script is intended to list all active lists from the data/
# directory for easier imports from external sources....
# Happy havesting :)

truncate -s 0 "$CI_PROJECT_DIR/source.txt"

cd "$CI_PROJECT_DIR"

for z in `find nxdomain/ -type f -name "*.zone"`
do
	printf "$CI_PROJECT_URL/raw/master/$z\n" | sort -u >> "$CI_PROJECT_DIR/source.txt"
done

#tree $CI_PROJECT_DIR/source/

ls -lh $CI_PROJECT_DIR/
