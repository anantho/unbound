#!/usr/bin/env bash

# include this script in your unbound configuration files by adding this
# in your crontab
# wget giturl:/nxdomain.conf -o /etc/unbound/zones/nxdomains.conf && \
# unbound-control reload
set -e

WGET=`which wget`

#NAME=`${WGET} -qO- 'https://gitlab.com/my-privacy-dns/external-sources/hosts-sources/raw/master/sources.list' \
#		| awk -F '/' '/^#/{ next }; { printf("%s\n",$10) | "sort -u -i \
#		| uniq -u " }'`
#
# make the right directories
for n in `wget -qO- 'https://raw.githubusercontent.com/mypdns/hosts-sources/master/sources.list' | awk -F '/' '/^#/{ next }; { printf("%s\n",$10) | "sort | uniq -u" }'`
do
	mkdir -p "${CI_PROJECT_DIR}/nxdomain/${n}"

	for lines in `${WGET} -qO- 'https://raw.githubusercontent.com/mypdns/hosts-sources/master/sources.list'`
	do
		${WGET} -qO- ${lines} | awk '/^#/{ next }; { if ( $1 ~ /[a-z]/ ) printf("local-zone: \"%s\" always_nxdomain\n",$1) | "sort -u -i | uniq -u" }' > "${CI_PROJECT_DIR}/nxdomain/${n}/${n}.zone"
	done

done

ls -lha "${CI_PROJECT_DIR}/nxdomain/"

# generate the NXDOMAIN Zone files
#for lines in `${WGET} -qO- 'https://gitlab.com/my-privacy-dns/external-sources/hosts-sources/raw/master/sources.list'`
#do
#	`${WGET} -qO- ${lines} | awk '/^#/{ next }; { if ( $1 ~ /[a-z]/ ) \
#		printf("local-zone: \"%s\" always_nxdomain\n",$1) | "sort -u -i \
#		| uniq -u " }'` > "${CI_PROJECT_DIR}/nxdomain/${NAME}/${NAME}.zone"
#
#done
