#!/usr/bin/env bash

# This is here we do the deployment not the afterscript

set -e

GIT=`which git`

#${GIT} remote set-url origin https://git:$MypDNS_CI@gitlab.com/$CI_PROJECT_PATH.git
${GIT} add .
${GIT} status
${GIT} commit -m '[skip ci] commit from CI runner'
${GIT} push -u origin master
