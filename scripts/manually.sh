#!/usr/bin/env bash

# Until scripts/nxdomain.sh is getting up and running this will be the manually
# script used for importing and converting into unbound always_nxdomain

# Source for output is:
# https://gitlab.com/my-privacy-dns/external-sources/hosts-sources/raw/master/sources.list

#export LC_ALL=C

set -e

#cd ""

#rm -fr "nxdomain/"
find nxdomain/ -type f -name "*.zone" -delete

sourceUrl="https://gitlab.com/my-privacy-dns/external-sources/hosts-sources/raw/master/data"
matrixUrl="https://gitlab.com/my-privacy-dns/matrix/matrix/raw/master/source"
awkCommand=(awk '{ printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }')

for n in `wget -qO- 'https://gitlab.com/my-privacy-dns/external-sources/hosts-sources/raw/master/sources.list' | awk -F '/' '{ printf("%s\n", tolower($10)) }'`
do
    mkdir -p "nxdomain/${n[@]}"

done

mkdir -p "nxdomain/fademind/" "nxdomain/phishing.database/" "nxdomain/blocklist/" \
    "nxdomain/hphosts/" "nxdomain/mitchellkrogza/phishing.database/" \
    "nxdomain/windowsspyblocker_extra/" "nxdomain/notrack/blocklists/" \
    "nxdomain/notrack/malware/" "nxdomain/suspiciousdomains"

wget -qO- "${sourceUrl}/CoinBlockerLists/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/coinblockerlists/coinblockerlists.zone"
wget -qO- "${sourceUrl}/abuse.ch/ransomware/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/abuse.ch/ransomware.zone"
wget -qO- "${sourceUrl}/abuse.ch/urlhaus/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/abuse.ch/urlhaus.zone"
wget -qO- "${sourceUrl}/badd_boyz_hosts/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/badd_boyz_hosts/badd_boyz_hosts.zone"
wget -qO- "${sourceUrl}/bambenekconsulting/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/bambenekconsulting/bambenekconsulting.zone"
wget -qO- "${sourceUrl}/BBcan177_MS-2/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/bbcan177_ms-2/bbcan177_ms-2.zone"
wget -qO- "${sourceUrl}/BBcan177_MS-4/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/bbcan177_ms-4/bbcan177_ms-4.zone"
wget -qO- "${sourceUrl}/blocklist_ads/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/blocklist/ads.zone"
wget -qO- "${sourceUrl}/blocklist_fraud/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/blocklist/fraud.zone"
wget -qO- "${sourceUrl}/blocklist_malware/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/blocklist/malware.zone"
wget -qO- "${sourceUrl}/blocklist_phising/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/blocklist/phising.zone"
wget -qO- "${sourceUrl}/blocklist_ransomeware/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/blocklist/ransomeware.zone"
wget -qO- "${sourceUrl}/blocklist_redirect/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/blocklist/redirect.zone"
wget -qO- "${sourceUrl}/blocklist_scam/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/blocklist/scam.zone"
wget -qO- "${sourceUrl}/blocklist_spam/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/blocklist/spam.zone"
wget -qO- "${sourceUrl}/blocklist_tracking/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/blocklist/tracking.zone"
wget -qO- "${sourceUrl}/cedia/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/cedia/cedia.zone"
wget -qO- "${sourceUrl}/dg-ads/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/dg-ads/dg-ads.zone"
wget -qO- "${sourceUrl}/dg-malicious/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/dg-malicious/dg-malicious.zone"
wget -qO- "${sourceUrl}/fademind_add_risk/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/fademind/add_risk.zone"
wget -qO- "${sourceUrl}/fademind_add_spam/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/fademind/add_spam.zone"
wget -qO- "${sourceUrl}/fademind_antipopads/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/fademind/antipopads.zone"
wget -qO- "${sourceUrl}/fademind_blocklists-facebook/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/fademind/blocklists-facebook.zone"
wget -qO- "${sourceUrl}/fademind_extras/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/fademind/extras.zone"
wget -qO- "${sourceUrl}/hphosts_ad_servers/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/hphosts/ad_servers.zone"
wget -qO- "${sourceUrl}/hphosts_emd/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/hphosts/emd.zone"
wget -qO- "${sourceUrl}/hphosts_exp/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/hphosts/exp.zone"
wget -qO- "${sourceUrl}/hphosts_fsa/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/hphosts/fsa.zone"
wget -qO- "${sourceUrl}/hphosts_grm/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/hphosts/grm.zone"
wget -qO- "${sourceUrl}/hphosts_hjk/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/hphosts/hjk.zone"
wget -qO- "${sourceUrl}/hphosts_mmt/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/hphosts/mmt.zone"
wget -qO- "${sourceUrl}/hphosts_psh/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/hphosts/psh.zone"
wget -qO- "${sourceUrl}/hphosts_pup/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/hphosts/pup.zone"
wget -qO- "${sourceUrl}/joewein/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/joewein/joewein.zone"
wget -qO- "${sourceUrl}/malwaredomainlist/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/malwaredomainlist/malwaredomainlist.zone"
wget -qO- "${sourceUrl}/mitchellkrogza/phishing.database/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mitchellkrogza/phishing.database/phishing.database.zone"
wget -qO- "${sourceUrl}/adaway/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/adaway/mobileadtrackers.zone"
wget -qO- "${sourceUrl}/mvps/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mvps/mvps.zone"
wget -qO- "${sourceUrl}/notrack/blocklists/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' > "nxdomain/notrack/blocklists/notrack-blocklists.zone"
wget -qO- "${sourceUrl}/notrack/malware/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' > "nxdomain/notrack/malware/notrack-malware.zone"
wget -qO- "${sourceUrl}/phishing_army_blocklist_extended/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/phishing_army_blocklist_extended/phishing_army_blocklist_extended.zone"
wget -qO- "${sourceUrl}/someonewhocares/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/someonewhocares/someonewhocares.zone"
wget -qO- "${sourceUrl}/suspiciousdomains_high/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/suspiciousdomains/suspiciousdomains_high.zone"
wget -qO- "${sourceUrl}/suspiciousdomains_low/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/suspiciousdomains/suspiciousdomains_low.zone"
wget -qO- "${sourceUrl}/suspiciousdomains_medium/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/suspiciousdomains/suspiciousdomains_medium.zone"
wget -qO- "${sourceUrl}/the-big-list-of-hacked-malware-web-sites/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/the-big-list-of-hacked-malware-web-sites/the-big-list-of-hacked-malware-web-sites.zone"
wget -qO- "${sourceUrl}/windowsspyblocker/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/windowsspyblocker/windowsspyblocker.zone"
wget -qO- "${sourceUrl}/windowsspyblocker_extra/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/windowsspyblocker_extra/windowsspyblocker_extra.zone"
wget -qO- "${sourceUrl}/xorcan/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/xorcan/xorcan.zone"
wget -qO- "${sourceUrl}/yoyo.org/domain.list" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/yoyo.org/yoyo.org.zone"
#wget -qO- "${sourceUrl}/malwaredomains/domain.list" | awk '{ printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/malwaredomains/malwaredomains.zone"


mkdir -p "nxdomain/mypdns/"

wget -qO- "${matrixUrl}/adware/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.adware.zone"
wget -qO- "${matrixUrl}/coin-blocker/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.coin-blocker.zone"
wget -qO- "${matrixUrl}/fake-news/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.fake-news.zone"
wget -qO- "${matrixUrl}/gambling/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.gambling.zone"
wget -qO- "${matrixUrl}/malicious/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.malicious.zone"
wget -qO- "${matrixUrl}/porno-sites/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.porno-sites.zone"
wget -qO- "${matrixUrl}/redirector/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.redirector.zone"
wget -qO- "${matrixUrl}/scamming/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.scamming.zone"
wget -qO- "${matrixUrl}/sharked-domains/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.sharked-domains.zone"
wget -qO- "${matrixUrl}/spyware/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.spyware.zone"
wget -qO- "${matrixUrl}/tracking/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.tracking.zone"
wget -qO- "${matrixUrl}/typosquatting/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" always_nxdomain\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.typosquatting.zone"
wget -qO- "${matrixUrl}/whitelist/combined.txt" | awk '/^(#|$)/{ next }; { printf("local-zone: \"%s\" transparent\n",tolower($1)) }' >> "nxdomain/mypdns/mypdns.whitelist.zone"
