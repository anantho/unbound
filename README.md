# Unbound

The zone files to be used with [Unbound](https://nlnetlabs.nl/projects/unbound/about/) 
From NLNetLabs

## zone format
This looks like a working code for NXDOMAIN and whitelist `www.example.net`

```python
local-zone: "example.net" always_nxdomain ; Blocks domain
local-zone: "www.example.net" transparent ; Whitelist domain
```

## About Unbound
Unbound is a validating, recursive, caching DNS resolver. It is designed to be 
fast and lean and incorporates modern features based on open standards.

To help increase online privacy, Unbound supports DNS-over-TLS which allows 
clients to encrypt their communication. In addition, it supports various modern 
standards that limit the amount of data exchanged with authoritative servers. 
These standards do not only improve privacy but also help making the DNS more 
robust. The most important are Query Name Minimisation, the Aggressive Use of 
DNSSEC-Validated Cache and support for authority zones, which can be used to 
load a copy of the root zone.

# DNS-over-TLS
We would be proud to annonce that we fully support DNS-over-TLS

But unfortunally we don't as a SSL Certificat that includes IP addresses cost
a shitload of money.

If you prefer to setup your unbound DNS recursor to use our DNS with the RPZ 
zones you can do this on the following way (Most Debian and Ubuntu variants)

```pyhton
# Within the server directives:
server:
    tls-cert-bundle: /etc/ssl/certs/ca-certificates.crt

# Next ad the forwarders
python:
forward-zone:
    name: "."
    forward-addr: 116.203.32.67@53 # ns1.dns.mypdns.org
    forward-addr: 95.216.209.53@53 # ns2.dns.mypdns.org
    forward-addr: 2a01:4f9:c010:410e::53@53 #ns0.dns.mypdns.org
    forward-addr: 2a01:4f8:1c0c:5f61::53@53 #ns1.dns.mypdns.org
    forward-ssl-upstream: yes
```
